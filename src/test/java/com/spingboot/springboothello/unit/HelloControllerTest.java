package com.spingboot.springboothello.unit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelloControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    public void hello_test() throws Exception{
        ResponseEntity<String> response = template.getForEntity("/", String.class);
        System.out.println(response.getStatusCode());
    }
}
