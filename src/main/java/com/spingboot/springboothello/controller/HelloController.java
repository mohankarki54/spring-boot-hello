package com.spingboot.springboothello.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String welcome(){
        return "<h3><b>Hello, Welcome to Spring boot</b></h3>";
    }
}
